import { Link } from "react-router-dom";
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

 function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
            <Typography variant ="h5">
                ยินดีต้อนรับสู่เว็ปพยากรณ์ดวง:
        
                &nbsp; &nbsp;  
                <Link to="/">พยากรณ์ดวง</Link>
                &nbsp; &nbsp;
                <Link to="/about">ผู้จัดทำ</Link>
            </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Header;