import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function AboutUs(props){



    return(

            <Box sx={{width:"25%"}}>
                <Paper elevation={3}>
                    <h2> จัดทำโดย: {props.name} </h2>
                    <h3> ติดต่อ ติณณภพ ได้ที่ {props.address} </h3>
                </Paper>
            </Box>
    
    );


}

export default AboutUs;