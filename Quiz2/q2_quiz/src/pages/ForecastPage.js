import ForecastResult from '../components/ForecastResult';
import { useState } from "react";
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Typography } from '@mui/material';

function ForecastPage(){

    const [name, setName] = useState("");
    const [forecastResult, setForecastResult] = useState(0);
    const [translateResult, setTranslateResult] = useState("");

    const [ month, setMonth] = useState("");
    const [ age, setAge] = useState("");

    function calculateLuck() {
        let m = parseFloat(month);
        let a = parseFloat(age);
        let luck =  (m*a)/20;
        setForecastResult(luck);
        if (luck >= 5) {
            setTranslateResult("ดวงดี")
        }else{
            setTranslateResult("ดวงไม่ดี")
        }

    }

    return(
                <Grid container spacing={2} sx={{ marginTop:"20px" }}>
                    <Grid item xs={12}>
                        <Typography variant="h4">
                            ยินดีต้อนรับสู่เว็ปทำนายดวง
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="h6">
                            คุณชื่อ: <input type="text" 
                                value ={name}
                                onChange={ (e) => { setName(e.target.value); }} /> <br /> 
                            เดือนเกิด: <input type="text" 
                                value ={month}
                                onChange={ (e) => { setMonth(e.target.value); }}/> <br /> 
                            อายุ: <input type="text" 
                                value ={age}
                                onChange={ (e) => { setAge(e.target.value); }}/> <br /> <br />

                            <Button variant="contained" onClick={   ()=>{   calculateLuck()  }   }>ทำนาย</Button>
                
                                { forecastResult != 0 &&
                                <div>
                                    <hr />
                                    นี่ผลคำนวณจ้า
                                    <ForecastResult
                                        name= { name }
                                        luck = { translateResult }
                                        result= { forecastResult }
                                    />
                                </div>
                                }
                        </Typography>
                    </Grid>
                </Grid>    
                            

    );
}

export default ForecastPage;