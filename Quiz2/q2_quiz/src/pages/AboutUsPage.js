import AboutUs from '../components/AboutUs';
import Paper from '@mui/material/Paper';

function AboutUsPage() {


    return(
        <div align="center">
            <h2> คณะผู้จัดทำเว็ป</h2>
            <AboutUs name = 'ติณณภพ' 
                  address='หาดใหญ่'/>
            <hr />
        </div>
    );

    
}

export default AboutUsPage;