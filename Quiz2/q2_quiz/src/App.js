import logo from './logo.svg';
import './App.css';
import { Routes, Route, Link } from "react-router-dom";
import Header from './components/Header';
import AboutUsPage from './pages/AboutUsPage';
import ForecastPage from './pages/ForecastPage';

import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
          <Route path ="about" element= {
            <AboutUsPage />
          } />

          
          <Route path ="/" element= {
            <ForecastPage />
          } />
          
      </Routes>
          
    </div>
  );
}

export default App;
